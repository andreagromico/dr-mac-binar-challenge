import React, { useContext } from 'react';
import {AuthContext}from './firebaseAuthContext';
import {Route, useNavigate} from 'react-router-dom';

export default function ProtectedRoute(props){
   let Navigate = useNavigate
    const authValue=useContext(AuthContext)
    if (authValue.userDataPresent){
        if(authValue.user==null){
            return(<Navigate to={props.redirectTo}></Navigate>)
        }
        else{
            return(
            
            <Route exact path={props.path}>
                {props.children}

            </Route>)
        }
    }
    else{
        
        return null
    }
}
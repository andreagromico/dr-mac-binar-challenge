import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import ProtectedRoute from "./libs/protectedRoute";

import Login from './pages/Login'
import Register from './pages/Register'
import Home from "./pages/home";
import Game from "./pages/Game";
import GameList from "./pages/GameList";
import GameDetails from "./pages/GameDetails";
import Homepageplayer from "./pages/Homepageplayer";
import Profile from "./pages/Profile";


function App() {
  return ( 
  <Router>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/register" element={<Register/>} />
            <Route exact path="/game" element={<Game />} />
            <Route exact path="/homepageplayer" element={<Homepageplayer />} />
            <Route exact path="/profile" element={<Profile />} />
            <Route exact path="/gamelist" element={<GameList />} />
            <Route exact path="/gamedetails" element={<GameDetails />} />
          </Routes>
  </Router>
  
  );

}

export default App;
import React from 'react';
import '../App.css';

class Home extends React.Component {
    render(){
        return(
            /* NAVIGATOR */
            <>
                <div className="jumbotron1">
                    <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top bg-opacity-75">
                        <div className="container-fluid">
                            <a className="navbar-brand" href="/">FSW Group 1</a>
                            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarNav">
                                <ul className="navbar-nav">
                                    <li className="nav-item">
                                        <a className="nav-link active fw-bold " aria-current="page" href="/">HOME</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link text fw-bold" href="GameList">GAME LIST</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link fw-bold" href="Profile">PROFILE</a>
                                    </li>
                                    <li className="nav-item2">
                                        <a className="nav-link fw-bold" href="Register">SIGN UP</a>
                                    </li>
                                    <li className="nav-item2">
                                        <a className="nav-link fw-bold" href="Login">LOGIN</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>

                    <h1 className="display-4">PLAY TRADITIONAL GAME</h1>
                    <p className="lead">Experience new traditional game play</p>
                    <a className="btn btn-warning btn-lg" href="Login" role="button">PLAY NOW</a>

                </div>
                
                {/* Halaman 2 */}
                <section id="scroll">
            <div className="container1 px-5">
                <div className="row gx-5 align-items-center">
                    <div className="col-lg-6 order-lg-2">
                        <div className="p-5"><img className="img-fluid rounded-circle" src="assets/image/01.jpg" alt="..." /></div>
                    </div>
                    <div className="col-lg-6 order-lg-1">
                        <div className="p-5">
                            <h2 className="display-5">Rock Paper Scissors</h2>
                            <p className="display-10">Rock paper scissors (also known by other orderings of the three items, with "rock" sometimes being called "stone", or as Rochambeau, roshambo, or ro-sham-bo)  is a hand game, usually played between two people, in which each player simultaneously forms one of three shapes with an outstretched hand. These shapes are "rock" (a closed fist), "paper" (a flat hand), and "scissors" (a fist with the index finger and middle finger extended, forming a V). "Scissors" is identical to the two-fingered V sign (also indicating "victory" or "peace") except that it is pointed horizontally instead of being held upright in the air.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section className="sistem-req">
    <div className="sub-judul-sysreq">
      <h5 className="special-3 text-white">Can My Computer Run This Game?</h5>
    </div>
    <div className="row">
      <div className="col-lg-5">
        <h1 className="sysreq-title text-white">SYSTEM REQUIREMENTS</h1>
        <table className="table table-bordered table-content-sys">
          <tbody>
            <tr>
              <td>
                <h2 className="text-warning">OS:</h2>
                <p className="requirements-info-content">
                  Windows 7 64-bit only<br/>(No OSX support at this time)
                </p>
              </td>
              <td>
                <h2 className="requirements-info text-warning">Processor:</h2>
                <p className="requirements-info-content">Intel Core 2 Duo @2.4GHz or AMD Athlon X2 @ 2.8GHz</p>
              </td>
            </tr>
            <tr>
              <td>
                <h2 className="requirements-info text-warning">Memory:</h2>
                <p className="requirements-info-content">4 GB RAM</p>
              </td>
              <td>
                <h2 className="requirements-info text-warning">Storage:</h2>
                <p className="requirements-info-content">8 GB available space</p>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <h2 className="requirements-info text-warning">Graphics:</h2>
                <p className="requirements-info-content">
                  NVIDIA GeForce GTX 660 2GB or<br/>AMD Radeon HD 7850 2GB
                  DirectX11 (Shader Model S)
                </p>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
</section>

<footer className="footer py-4">
        <div className="container">
            <div className="row align-items-center">
                <div className="col-lg-4 text-lg-start">Copyright &copy; Your Website 2021</div>
                <div className="col-lg-4 my-3 my-lg-0">
                    <a className="btn btn-dark btn-social mx-2" href="#!"><i className="fab fa-twitter"></i></a>
                    <a className="btn btn-dark btn-social mx-2" href="#!"><i className="fab fa-facebook-f"></i></a>
                    <a className="btn btn-dark btn-social mx-2" href="#!"><i className="fab fa-linkedin-in"></i></a>
                </div>
                <div className="col-lg-4 text-lg-end">
                    <a className="link-dark text-decoration-none me-3" href="#!">Privacy Policy</a>
                    <a className="link-dark text-decoration-none" href="#!">Terms of Use</a>
                </div>
            </div>
        </div>
    </footer>

        </>
            );
    }
}

export default Home;
import React, { Component, Fragment } from "react";
import './Game.css';
import batu from '../assets/games/batu.png'
import kertas from '../assets/games/kertas.png'
import gunting from '../assets/games/gunting.png'
import refresh from '../assets/games/refresh.png'
import NavbarTop from '../components/NavbarTop'
import 'bootstrap/dist/css/bootstrap.min.css'
// let play = () => console.log('batu')
// let playGunting = () => console.log('gunting')
// let playKertas = () => console.log('kertas')
// import {Helmet} from "react-helmet";
// import GameSuit from "../assets/js/game-suit";
let resultName = 'result';


class Game extends Component{

    constructor(props) {
        super(props);
        this.state = {boxColor: '',boxColor2:'',boxColor3:''};
        this.state = {result: "VS"}
        
     
      }

      confirmation(){
        window.confirm('Apakah ingin mengulang permainan ?');
      }


      refresh() {
        this.setState(() => ({
            boxColor:  '',
            boxColor2:  '',
            boxColor3:  '',
            boxColor4:  '',
            boxColor5:  '',
            boxColor6:  '',
            result: 'VS',
            clickEvent: '', 
            clickEvent2: '' ,
            clickEvent3: '',
            
          }));
          resultName= 'result'
      }
    
    playerChoose = (player) => {
                
        const comShuffle = ['batu', 'kertas', 'gunting'];
        this.comChoose = comShuffle[Math.floor(Math.random() * comShuffle.length)];

        if(this.comChoose === 'batu'){
            this.setState(() => ({
                boxColor4:  'gold',
                
              }));
        }else if(this.comChoose === 'kertas'){
            this.setState(() => ({
                boxColor5:  'gold',
                
              }));
        }else{
            this.setState(() => ({
                boxColor6:  'gold',
               
              }));
        }

        if(player === 'batu'){
            this.setState(() => ({
                boxColor:  'gold',
                clickEvent: 'none', 
                clickEvent2: 'none' ,
                clickEvent3: 'none' 
              }));
        }else if (player === 'kertas'){
            this.setState(() => ({
                boxColor2:  'gold',
                clickEvent: 'none', 
                clickEvent2: 'none' ,
                clickEvent3: 'none' 
              }));
        }else{
            this.setState(() => ({
                boxColor3:  'gold',
                clickEvent: 'none', 
                clickEvent2: 'none' ,
                clickEvent3: 'none' 
              }));
        }

        
        if(player === this.comChoose){
            this.setState({ result: "draw"});
            resultName = 'draw'

            
          
        }else if (
            (player === "batu" && this.comChoose === "gunting") || (player === "gunting" && this.comChoose === "kertas") || (player === "kertas" && this.comChoose === "batu")
            ){
        
                this.setState({ result: "Pwin"}); 
                resultName = 'pwin'
                
        }else{
            
            this.setState({ result: "Com Win"});
            resultName = 'cwin'
   
        }
        

      };
    
      
    render(){

        return (
  

            <Fragment>
                <>
                <NavbarTop sticky="top" />                 
                </>

                                 
                        
                <div className="container-game">
                    <div className="wrapper wrapper-3">
                        <div className="user">

                            <div className="box-4">
                                <p>PLAYER</p>
                            </div>

                            <div onClick={() => {this.playerChoose("batu")}} className="box-3" style={{ backgroundColor: this.state.boxColor, pointerEvents: this.state.clickEvent }} id="batu-p">
                                <img className="suit" src={batu} alt="Batu" />
                            </div>

                            <div onClick={() => {this.playerChoose("kertas")}}  className="box-3" style={{ backgroundColor: this.state.boxColor2, pointerEvents: this.state.clickEvent2 }} id="kertas-p" >
                                <img className="suit" src={kertas} alt="kertas" />
                            </div>

                            <div onClick={() => {this.playerChoose("gunting")}} className="box-3" style={{ backgroundColor: this.state.boxColor3, pointerEvents: this.state.clickEvent3 }} id="gunting-p">
                                <img className="suit" src={gunting} alt="gunting" />    
                            </div>
                        </div>

                        <div className="mid">
                            <div className={resultName} id="hasil" >{ this.state.result }</div>
                        </div> 

                        <div className="com">
                            <div className="box-4">
                            <p>COM</p>
                            </div>
                            
                            <div  className="box-3" id="batu-c" style={{ backgroundColor: this.state.boxColor4 }} value='batu'>
                                <img className="suit" src={batu} alt="Batu" />
                            </div>

                            <div  className="box-3" style={{ backgroundColor: this.state.boxColor5 }} id="kertas-c" value='kertas'>
                                <img className="suit" src={kertas} alt="kertas" />
                            </div>

                            <div  className="box-3" style={{ backgroundColor: this.state.boxColor6 }} id="gunting-c" value='gunting'>
                                <img className="suit" src={gunting} alt="gunting" /> 
                            </div>   
                        </div>
                    </div>
                </div> 

                <div className="container-game">
                    <div className="wrapper wrapper-3">
                        <div className="refresh">
                            <div className="box-2" id="refresh">
                                <img className="ref-btn" onClick={() => {this.refresh()}} src={refresh} alt="gunting" />
                            </div>  
                        </div>   
                    </div>
                </div>

            </Fragment>
            
        )
    }
}

export default Game;
